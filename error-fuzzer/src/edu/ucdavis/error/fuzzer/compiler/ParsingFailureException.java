package edu.ucdavis.error.fuzzer.compiler;

import org.eclipse.cdt.core.dom.ast.IASTProblem;

import java.util.Arrays;

public class ParsingFailureException extends Exception {

  /** */
  private static final long serialVersionUID = 1L;

  private final IASTProblem[] problems;

  public ParsingFailureException(IASTProblem[] problems) {
    super(
        Arrays.stream(problems)
            .map(IASTProblem::getMessageWithLocation)
            .reduce("", (s1, s2) -> s1 + "\n" + s2));
    this.problems = problems;
  }

  public ParsingFailureException(Throwable arg0) {
    super(arg0);
    this.problems = new IASTProblem[0];
  }
}
