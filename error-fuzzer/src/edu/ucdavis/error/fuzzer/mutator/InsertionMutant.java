package edu.ucdavis.error.fuzzer.mutator;

import java.util.ArrayList;

import com.google.common.collect.ImmutableList;

import edu.ucdavis.error.fuzzer.token.TokenizedProgram;
import edu.ucdavis.error.fuzzer.token.segment.EmptyProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;

/**
 * this mutator can only be applied to SeedMutant.
 * 
 * @author neo
 *
 */
public class InsertionMutant extends AbstractMutant {

  private final int index;

  private final IProgramSegment token;

  public InsertionMutant(ImmutableList<IProgramSegment> seed, int index,
                         TokenProgramSegment token) {
    super(new SeedMutant(seed));
    this.index = index;
    this.token = token;
  }

  public InsertionMutant(AbstractMutant mutant, int index,
                         IProgramSegment token) {
    super(mutant);
    this.index = index;
    this.token = token;
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedSeed() {
    ArrayList<IProgramSegment> mutant = this.chain.getAlignedSeed();
    mutant.add(this.index, EmptyProgramSegment.EMPTY_TOKEN);
    return mutant;
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedMutant() {
    ArrayList<IProgramSegment> mutant = this.chain.getAlignedSeed();
    mutant.add(this.index, this.token);
    return mutant;
  }

}
