package edu.ucdavis.error.fuzzer.mutator;

import java.util.ArrayList;

import com.google.common.collect.ImmutableList;

import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;

public class SwapMutant extends AbstractMutant {

  private final int index1;

  private final int index2;

  public SwapMutant(ImmutableList<IProgramSegment> seed, int index1,
                           int index2) {
    this(new SeedMutant(seed), index1, index2);
  }

  public SwapMutant(AbstractMutant chain, int index1,
      int index2) {
    super(chain);
    this.index1 = index1;
    this.index2 = index2;
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedSeed() {
    ArrayList<IProgramSegment> mutant = this.chain.getAlignedSeed();
    return mutant;
  }

  @Override
  public ArrayList<IProgramSegment> getAlignedMutant() {
    ArrayList<IProgramSegment> mutant = this.chain.getAlignedMutant();
    IProgramSegment segment1 = mutant.get(index1);
    IProgramSegment segment2 = mutant.get(index2);
    mutant.set(index2, segment1);
    mutant.set(index1, segment2);
    return mutant;
  }

}
