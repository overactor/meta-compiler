package edu.ucdavis.error.fuzzer.mutator;

import java.util.ArrayList;

import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.TokenUtility;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;

public abstract class AbstractMutant {

  protected final AbstractMutant chain;

  public AbstractMutant(AbstractMutant chain) {
    this.chain = chain;
  }

  public abstract ArrayList<IProgramSegment> getAlignedSeed();

  public abstract ArrayList<IProgramSegment> getAlignedMutant();

  public final String getAlignedSeedAsString() {
    return TokenUtility.s()
        .convertTokenSequenceToProgram(this.getAlignedSeed());
  }

  public final String getAlignedMutantAsString() {
    return TokenUtility.s()
        .convertTokenSequenceToProgram(this.getAlignedMutant());
  }

}
