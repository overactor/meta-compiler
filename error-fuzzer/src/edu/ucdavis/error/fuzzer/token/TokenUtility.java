package edu.ucdavis.error.fuzzer.token;

import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class TokenUtility {

  private static final TokenUtility INSTANCE = new TokenUtility();

  public static TokenUtility s() {
    return INSTANCE;
  }

  /**
   * convert a token sequence to a program. Each line has a single token.
   */
  public String convertTokenSequenceToProgram(List<IProgramSegment> tokens) {
    final StringBuilder builder = new StringBuilder();
    tokens.forEach(t -> builder.append(t.getLexeme()).append("\n"));
    return builder.toString();
  }

  public String convertSegmentTypeToString(List<IProgramSegment> segments) {
    final StringBuilder builder = new StringBuilder();
    for (IProgramSegment segment : segments) {
      if (segment instanceof TokenProgramSegment) {
        builder.append(((TokenProgramSegment) segment).getTokenType());
      }
      builder.append('\n');
    }
    return builder.toString();
  }

  public void convertTokenSequenceToProgram(File file, List<IProgramSegment> tokens)
      throws IOException {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      writer.write(this.convertTokenSequenceToProgram(tokens));
    }
  }
}
