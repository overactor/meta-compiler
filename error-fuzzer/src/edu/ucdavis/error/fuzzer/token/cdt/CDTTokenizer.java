package edu.ucdavis.error.fuzzer.token.cdt;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import edu.ucdavis.error.fuzzer.compiler.ParsingFailureException;
import edu.ucdavis.error.fuzzer.compiler.SourceFile;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import org.eclipse.cdt.core.dom.ast.ASTVisitor;
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException;
import org.eclipse.cdt.core.dom.ast.IASTExpression;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.parser.IToken;
import org.pmw.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;

public class CDTTokenizer {

  private static class UsageVisitor extends ASTVisitor {

    final Int2ObjectArrayMap<TokenPair> map;

    public UsageVisitor(final Int2ObjectArrayMap<TokenPair> map) {
      super();
      this.shouldVisitExpressions = true;
      this.map = map;
    }

    @Override
    public int visit(IASTExpression expression) {
      try {
        for (IToken token = expression.getSyntax(); token != null; token = token.getNext()) {
          final int exprOffset = expression.getFileLocation().getNodeOffset();
          final int tokenOffset = exprOffset + token.getOffset();
          TokenPair tokenPair = map.get(tokenOffset);
          assert (tokenPair.token.getOffset() == tokenOffset);
          assert (tokenPair != null);
          tokenPair.usage = true;
        }
      } catch (ExpansionOverlapsBoundaryException e) {
        //        throw e;
        Logger.warn(e);
      }
      return ASTVisitor.PROCESS_SKIP;
    }
  }

  public static class TokenPair {

    public final IToken token;

    // private EnumIdentifierUseDef usage;
    private boolean usage;

    public TokenPair(IToken token) {
      this.token = token;
      this.usage = false;
    }

    //    public int computeType() {
    //      switch (token.getType()) {
    //      case IToken.tIDENTIFIER:
    //        return this.usage ? EnumTokenType.IDENTIFIER_USE
    //            : EnumTokenType.IDENTIFIER_DEF;
    //      default:
    //        // return EnumTokenType.OTHER;
    //        return EnumTokenType.getDefaultTokenType();
    //      }
    //    }

  }

  private final IASTTranslationUnit unit;

  private final ImmutableList<TokenProgramSegment> tokens;

  private final ImmutableList<TokenPair> rawTokens;

  public CDTTokenizer(SourceFile sourceFile)
      throws ParsingFailureException, ExpansionOverlapsBoundaryException {
    unit = new CDTParser().parse(sourceFile);
    rawTokens = this.parseTokens(this.unit);
    computeTokenUsage(rawTokens);
    tokens = convertRawTokens(rawTokens);
  }

  public ImmutableList<TokenPair> getRawTokens() {
    return rawTokens;
  }

  public ImmutableList<TokenProgramSegment> getTokens() {
    return tokens;
  }

  private static ImmutableList<TokenProgramSegment> convertRawTokens(
      ImmutableList<TokenPair> rawTokens) {
    final Builder<TokenProgramSegment> builder = ImmutableList.builder();
    for (int i = 0, size = rawTokens.size(); i < size; ++i) {
      final IToken token = rawTokens.get(i).token;
      if (isGtOperator(token)) {
        List<IToken> gtOperatorList = new ArrayList<>();
        gtOperatorList.add(token);
        while ((i + 1) < size && isGtOperator(rawTokens.get(i + 1).token)) {
          gtOperatorList.add(rawTokens.get(++i).token);
        }
        arrangeGtOperatorsAndAddToBuilder(gtOperatorList, builder);
        continue;
      }
      builder.add(convertRawTokens(token));
    }
    return builder.build();
  }

  private static void arrangeGtOperatorsAndAddToBuilder(
      List<IToken> gtOperatorList, Builder<TokenProgramSegment> builder) {
    if (gtOperatorList.size() == 1) {
      builder.add(convertRawTokens(gtOperatorList.get(0)));
      return;
    }
    final List<Integer> offsets = new ArrayList<>();
    for (IToken gt : gtOperatorList) {
      final String lexeme = gt.getImage();
      if (lexeme.equals(">>")) {
        offsets.add(gt.getOffset());
        offsets.add(gt.getOffset() + 1);
      } else if (lexeme.equals(">")) {
        offsets.add(gt.getOffset());
      } else {
        throw new AssertionError("Unhandled token " + lexeme);
      }
    }
    for (int i = 0, size = offsets.size(); i < size; ++i) {
      final int offset = offsets.get(i);
      if (i + 1 < size) {
        final int endOffset = offsets.get(++i) + 1;
        //        final StringBuilder lexemeBuilder = new StringBuilder();
        //        lexemeBuilder.append('>');
        //        IntStream.range(offset + 1, endOffset - 1).forEach(value -> lexemeBuilder.append(' '));
        //        lexemeBuilder.append('>');
        builder.add(TokenProgramSegment.createTokenWithNoOffsets(">>", IToken.tSHIFTR));
      } else {
        builder.add(TokenProgramSegment.createTokenWithNoOffsets(">", IToken.tGT));
      }
    }
  }

  private static TokenProgramSegment convertRawTokens(IToken token) {
    return TokenProgramSegment.createTokenWithValidOffsets(
        token.getImage(), token.getType(), token.getOffset(), token.getEndOffset());
  }

  private static boolean isGtOperator(IToken token) {
    final String lexeme = token.getImage();
    if (lexeme.equals(">>")) {
      checkState(token.getType() == IToken.tSHIFTR);
      return true;
    }
    if (lexeme.equals(">")) {
      checkState(token.getType() == IToken.tGT);
      return true;
    }
    return false;
  }

  private void computeTokenUsage(ImmutableList<TokenPair> tokens) {
    final Int2ObjectArrayMap<TokenPair> map = new Int2ObjectArrayMap<>();
    tokens.forEach(p -> map.put(p.token.getOffset(), p));
    // for (TokenPair p : tokens) {
    // System.out.println(p.token.toString() + " @ " + p.token.getOffset()
    // + ", class = " + p.token.getClass());
    // }

    this.unit.accept(new UsageVisitor(map));
  }

  private ImmutableList<TokenPair> parseTokens(IASTTranslationUnit unit)
      throws ExpansionOverlapsBoundaryException {
    Builder<TokenPair> builder = ImmutableList.builder();
    IToken token = unit.getSyntax();
    while (token != null) {
      builder.add(new TokenPair(token));
      token = token.getNext();
    }
    return builder.build();
  }
}
