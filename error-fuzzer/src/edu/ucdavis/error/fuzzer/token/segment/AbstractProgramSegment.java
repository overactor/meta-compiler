package edu.ucdavis.error.fuzzer.token.segment;

import com.google.common.base.Preconditions;

public abstract class AbstractProgramSegment implements IProgramSegment {

  protected final String lexeme;

  protected AbstractProgramSegment(String lexeme) {
    super();
    Preconditions.checkNotNull(lexeme);
    this.lexeme = lexeme;
  }

  @Override
  public final String getLexeme() {
    return this.lexeme;
  }

  public String toString() {
    return this.getLexeme();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    AbstractProgramSegment that = (AbstractProgramSegment) o;

    return lexeme.equals(that.lexeme);
  }

  @Override
  public int hashCode() {
    return lexeme.hashCode();
  }
}
