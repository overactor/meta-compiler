package edu.ucdavis.error.fuzzer.reducer;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class Edit {

  private static class AtomicEdit {

    private final int line;

    private final String oldValue;

    private final String newValue;

    public AtomicEdit(int line, String oldValue, String newValue) {
      super();
      this.line = line;
      this.oldValue = oldValue;
      this.newValue = newValue;
    }

    /**
     * 
     * @param tokens
     * @return true, if the application is successful.
     */
    public void apply(List<String> tokens) {
//      Preconditions.checkState(tokens.get(line).equals(oldValue),
//          "The expected old value at line %s is %s, however, now it is %s",
//          line, oldValue, tokens.get(line));
      tokens.set(line, newValue);
    }

  }

  public static Edit computeEdit(List<String> seed, List<String> mutant) {
    Preconditions.checkArgument(seed.size() == mutant.size(),
        "The seed and the mutant have different sizes");
    final ImmutableList.Builder<AtomicEdit> builder = ImmutableList.builder();
    final int size = seed.size();
    for (int i = 0; i < size; ++i) {
      final String inSeed = seed.get(i);
      final String inMutant = mutant.get(i);
      if (!inSeed.equals(inMutant)) {
        builder.add(new AtomicEdit(i, inSeed, inMutant));
      }
    }
    return new Edit(builder.build());
  }

  private final ImmutableList<AtomicEdit> edits;

  private final ImmutableSet<Integer> editedLines;

  private Edit(ImmutableList<AtomicEdit> edits) {
    super();
    this.edits = edits;
    this.editedLines = this.computeEditedLines(edits);
  }

  public ImmutableSet<Integer> getEditedLines() {
    return editedLines;
  }

  private ImmutableSet<Integer> computeEditedLines(
      ImmutableList<AtomicEdit> edits) {
    ImmutableSet.Builder<Integer> set = ImmutableSet.builder();
    edits.forEach(s -> set.add(s.line));
    return set.build();
  }

  public List<String> duplicateAndEdit(List<String> tokens) {
    List<String> result = new ArrayList<>(tokens);
    this.edits.forEach(e -> e.apply(result));
    return result;
  }

}