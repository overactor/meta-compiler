package edu.ucdavis.error.fuzzer.reducer.cdt;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * 
 * map between offset and line/columns
 * 
 * @author Chengnian Sun
 *
 */
public class FileLocationContext {

  public final static class LineAndColumn {

    private final int line;

    private final int column;

    public LineAndColumn(int line, int column) {
      this.line = line;
      this.column = column;
    }

    public int getLine() {
      return line;
    }

    public int getColumn() {
      return column;
    }
  }

  public static class OffsetRangeInALine {

    private final int startOffset;

    private final int endOffset;

    public OffsetRangeInALine(int startOffset, int endOffset) {
      super();
      this.startOffset = startOffset;
      this.endOffset = endOffset;
    }

    /**
     * 
     * @param offset
     * @return -1, offset < startOffset; <br>
     *         0, offset is within this range; <br>
     *         +1, offset > endOffset;
     */
    public int compareOffsetToRange(int offset) {
      if (offset < this.startOffset)
        return -1;
      if (offset > this.endOffset)
        return +1;
      return 0;
    }

    public int numberOfChars() {
      return this.endOffset - this.startOffset + 1;
    }

    public int getStartOffset() {
      return startOffset;
    }

    public int getEndOffset() {
      return endOffset;
    }

    public int mapToColumn(int offset) {
      Preconditions.checkArgument(offset >= this.startOffset
          && offset <= this.endOffset);
      return offset - this.startOffset + 1;
    }

  }

  private final String fileContent;

  /**
   * make sure this char array is not exposed.
   */
  private final char[] charArray;

  private final ImmutableList<OffsetRangeInALine> offsetMap;

  @Override
  public String toString() {
    return this.fileContent;
  }

  public int numberOfLines() {
    return this.offsetMap.size();
  }

  public LineAndColumn mapOffsetToLineAndColumn(int offset) {
    ImmutableList<OffsetRangeInALine> list = this.offsetMap;

    int fromIndex = 0;
    int toIndex = list.size() - 1;

    while (fromIndex <= toIndex) {
      int middleIndex = (fromIndex + toIndex) / 2;
      final OffsetRangeInALine middle = list.get(middleIndex);
      final int cmp = middle.compareOffsetToRange(offset);
      if (cmp == 0) {
        // return middle;
        final int column = middle.mapToColumn(offset);
        return new LineAndColumn(middleIndex + 1, column);
      } else if (cmp < 0) {
        toIndex = middleIndex - 1;
      } else {
        fromIndex = middleIndex + 1;
      }
    }
    return null;
  }

  /**
   * line starts from 1;
   * 
   * @param line
   * @return
   */
  public int getStartOffsetOfLine(int line) {
    return this.getOffsetRangeOfLine(line).getStartOffset();
  }

  /**
   * line starts from 1;
   * 
   * @param line
   * @return
   */
  public int getEndOffsetOfLine(int line) {
    return this.getOffsetRangeOfLine(line).getEndOffset();
  }

  /**
   * line starts from 1;
   * 
   * @param line
   * @return
   */
  public OffsetRangeInALine getOffsetRangeOfLine(int line) {
    Preconditions.checkArgument(line > 0 && line <= this.offsetMap.size());
    return this.offsetMap.get(line - 1);
  }

  public FileLocationContext(String fileContent) {
    this.fileContent = fileContent;
    this.charArray = fileContent.toCharArray();
    this.offsetMap = this.buildOffsetMap(this.charArray);
  }

  private ImmutableList<OffsetRangeInALine> buildOffsetMap(char[] charArray) {
    ImmutableList.Builder<OffsetRangeInALine> builder = ImmutableList.builder();
    int start;
    int end;
    final int length = charArray.length;
    for (int i = 0; i < length; ++i) {
      start = i;
      for (; i < length - 1; ++i) {
        final char c = charArray[i];
        if (c == '\n') {
          break;
        }
      }
      end = i;
      builder.add(new OffsetRangeInALine(start, end));
    }
    return builder.build();
  }
}
