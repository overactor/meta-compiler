package edu.ucdavis.error.fuzzer.core;

public class ErrorFuzzerFatalError extends Error {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ErrorFuzzerFatalError() {
    super();
  }

  public ErrorFuzzerFatalError(String message, Throwable cause,
      boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public ErrorFuzzerFatalError(String message, Throwable cause) {
    super(message, cause);
  }

  public ErrorFuzzerFatalError(String message) {
    super(message);
  }

  public ErrorFuzzerFatalError(Throwable cause) {
    super(cause);
  }

}
