package edu.ucdavis.error.fuzzer.util;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import java.io.*;
import java.util.ArrayList;
import java.util.Stack;

public class FileUtility {

  private static final ImmutableList<String> SOURCE_FILE_EXTS =
      ImmutableList.of(".c", ".C", ".cpp", ".cc");

  public static final FileFilter SOURCE_FILE_FILTER =
      new FileFilter() {

        @Override
        public boolean accept(File pathname) {
          final String name = pathname.getName();
          for (String ext : SOURCE_FILE_EXTS) {
            if (name.endsWith(ext)) {
              return true;
            }
          }
          return false;
        }
      };

  public static ArrayList<File> globSourceFiles(File root) {
    return globRegularFiles(root, SOURCE_FILE_FILTER);
  }

  public static ArrayList<File> globRegularFiles(File root, FileFilter filter) {
    Preconditions.checkArgument(root.exists(), "The folder %s does not exist", root);
    Preconditions.checkArgument(root.isDirectory(), "The file %s is not a directory", root);

    ArrayList<File> result = new ArrayList<>();
    Stack<File> worklist = new Stack<>();
    worklist.add(root);
    while (worklist.size() > 0) {
      File folder = worklist.pop();
      Preconditions.checkState(folder.isDirectory());
      for (File child : folder.listFiles()) {
        if (child.isDirectory()) {
          worklist.push(child);
        }
        if (child.isFile() && filter.accept(child)) {
          result.add(child);
        }
      }
    }
    return result;
  }

  public static String readFileAndDiscardWhitespaces(File file) {
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      final StringBuilder builder = new StringBuilder();
      int c;
      while ((c = reader.read()) >= 0) {
        if (Character.isWhitespace(c)) {
          continue;
        }
        builder.append(c);
      }
      return builder.toString();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
