package edu.ucdavis.error.fuzzer.repository;

/**
 * Created by neo on 5/7/17.
 */
public abstract class CompilerErrorRepositoryVisitor {

  public abstract  void visitErrorMessage(String errorMessage);

  public abstract void visitErrorInstance(ErrorInstance errorInstance);

}
