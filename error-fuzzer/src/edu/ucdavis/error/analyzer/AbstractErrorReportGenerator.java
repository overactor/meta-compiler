package edu.ucdavis.error.analyzer;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import edu.ucdavis.error.fuzzer.repository.CompilerErrorRepository;
import edu.ucdavis.error.fuzzer.repository.ErrorInstance;
import org.apache.commons.io.FileUtils;
import org.pmw.tinylog.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

/** Created by neo on 2/24/17. */
public abstract class AbstractErrorReportGenerator {

  protected static class PairExample {

    protected final String goodExample;

    protected final String badExample;

    protected final String errorMsg;

    protected final String errorId;

    public PairExample(String goodExample, String badExample, String errorMsg, String errorId) {
      super();
      this.goodExample = goodExample;
      this.badExample = badExample;
      this.errorMsg = errorMsg;
      this.errorId = errorId;
    }
  }

  protected final ImmutableList<PairExample> pairs;

  protected AbstractErrorReportGenerator(File errorRepoFolder) {
    final CompilerErrorRepository repo =
        CompilerErrorRepository.loadExistingErrorRepository(errorRepoFolder, Integer.MAX_VALUE);

    ImmutableList.Builder<PairExample> builder = ImmutableList.builder();
    for (String errorMsg : repo.getSavedErrorMessages()) {
      for (ErrorInstance instance : repo.getSavedErrorInstances(errorMsg)) {
        if (instance.checkIntegrity().hasProblems()
            || !instance.getReducedFormattedGoodFile().isFile()
            || !instance.getReducedFormattedBadFile().isFile()) {
          continue;
        }
        final String msg = instance.readCompilerErrorMsg();
        final String errorId = instance.readCompilerErrorId();
        try {
          final String good =
              FileUtils.readFileToString(instance.getReducedFormattedGoodFile(), Charsets.UTF_8);
          final String bad =
              FileUtils.readFileToString(instance.getReducedFormattedBadFile(), Charsets.UTF_8);
          builder.add(new PairExample(good, bad, msg, errorId));
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
    this.pairs = builder.build();
    Logger.info("read {} example pairs.", this.pairs.size());
  }

  public void save(File file) {
    try (PrintStream stream = new PrintStream(file)) {
      writeHead(stream);

      for (int i = 0, pairs1Size = this.pairs.size(); i < pairs1Size; i++) {
        PairExample example = this.pairs.get(i);
        this.saveExample(i + 1, stream, example);
      }
      writeBottom(stream);
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  protected abstract void writeHead(PrintStream stream);

  protected abstract void writeBottom(PrintStream stream);

  protected abstract void saveExample(int id, PrintStream stream, PairExample example);
}
