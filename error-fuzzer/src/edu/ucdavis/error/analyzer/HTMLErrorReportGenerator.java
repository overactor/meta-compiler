package edu.ucdavis.error.analyzer;

import java.io.File;
import java.io.PrintStream;

import org.pmw.tinylog.Logger;

public class HTMLErrorReportGenerator extends  AbstractErrorReportGenerator {


  protected HTMLErrorReportGenerator(File errorRepoFolder) {
    super(errorRepoFolder);
  }

  @Override
  protected void writeHead(PrintStream s) {
    s.println("<html>");

    s.println("<head>");

    s.println("<style>");
    s.println(
            "table, th, td { border: 1px solid black; border-collapse: collapse; }");
    s.println("th, td { padding: 5px; text-align: left;}");
    s.println("</style>");

    s.println("<title>");
    s.println("Error examples for meta-compiler");
    s.println("</title>");

    s.println("</head>");

    s.println("<body>");
  }


  @Override
  protected void writeBottom(PrintStream s) {
    s.println("</body>");
    s.println("</html>");
  }

  public static void main(String[] args) {
    File expFolder = null;
    if (args.length == 0) {
      expFolder = new File("exp-runs/error/RandomTokenManipulationFuzzingEngine");
      Logger.info("Using default exp folder: {}", expFolder);
    } else if (args.length == 1){
      expFolder = new File(args[0]);
    } else {
      System.err.println(
          "<main> <error folder, eg.: exp-runs/error/IdentifierSubstituionFuzzingEngine");
      System.exit(1);
    }
    HTMLErrorReportGenerator gen = new HTMLErrorReportGenerator(expFolder);
    File outputFile = new File("test.report.html");
    gen.save(outputFile);
    Logger.info("saved report to {}", outputFile);

  }

  @Override
  protected void saveExample(int id, PrintStream s, PairExample example) {
    s.println("<table style='width:100%'>");

    s.println("<tr>");
    s.println("<td colspan='2' >");
    s.println("<strong>" + (id) + ": " + example.errorMsg + "</strong>");
    s.println("</td>");
    s.println("</tr>");

    s.println("<tr>");
    s.println("<td bgcolor='SKYBLUE'  style='width:50%'>");
    s.printf("<pre>%s</pre>\n", example.goodExample);
    s.println("</td>");

    s.println("<td bgcolor='LIGHTSALMON'  style='width:50%'>");
    s.printf("<pre>%s</pre>\n", example.badExample);
    s.println("</td>");
    s.println("</tr>");

    s.println("</table>");
  }
}
