# Error Fuzzer

This program mutates compilable programs.

## Installation

MacOS High Sierra

* [MacPorts](https://www.macports.org/install.php)

General

* Ant
  * `sudo port install apache-ant`
* Java Software Development Kit, jdk-9.0.4
	* http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html


