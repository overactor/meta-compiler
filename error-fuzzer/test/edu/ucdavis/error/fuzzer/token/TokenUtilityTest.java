package edu.ucdavis.error.fuzzer.token;

import com.google.common.collect.ImmutableList;
import com.google.common.truth.Truth;
import edu.ucdavis.error.fuzzer.token.segment.EmptyProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.IProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.NonTokenProgramSegment;
import edu.ucdavis.error.fuzzer.token.segment.TokenProgramSegment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

/** Created by Chengnian Sun on 5/1/17. */
@RunWith(JUnit4.class)
public class TokenUtilityTest {

  private static void testTypeConversion(String expected, List<IProgramSegment> segments) {
    Truth.assertThat(TokenUtility.s().convertSegmentTypeToString(segments)).isEqualTo(expected);
  }

  @Test
  public void testConvertSegmentTypeToString() {
    testTypeConversion(
        "1\n",
        ImmutableList.<IProgramSegment>builder()
            .add(TokenProgramSegment.createTokenWithNoOffsets("", 1))
            .build());
    testTypeConversion(
        "1\n2\n",
        ImmutableList.<IProgramSegment>builder()
            .add(TokenProgramSegment.createTokenWithNoOffsets("", 1))
            .add(TokenProgramSegment.createTokenWithNoOffsets("", 2))
            .build());
    testTypeConversion(
        "1\n\n2\n",
        ImmutableList.<IProgramSegment>builder()
            .add(TokenProgramSegment.createTokenWithNoOffsets("", 1))
            .add(EmptyProgramSegment.EMPTY_TOKEN)
            .add(TokenProgramSegment.createTokenWithNoOffsets("", 2))
            .build());
    testTypeConversion(
        "1\n\n2\n",
        ImmutableList.<IProgramSegment>builder()
            .add(TokenProgramSegment.createTokenWithNoOffsets("", 1))
            .add(new NonTokenProgramSegment(""))
            .add(TokenProgramSegment.createTokenWithNoOffsets("", 2))
            .build());
  }
}
