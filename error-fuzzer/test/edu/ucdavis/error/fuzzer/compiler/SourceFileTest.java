package edu.ucdavis.error.fuzzer.compiler;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

/**
 * Created by Chengnian Sun on 4/2/17.
 */
@RunWith(JUnit4.class)
public class SourceFileTest {

  @Test
  public void testCppFileExtensions() {
    assertTrue(SourceFile.isCppSourceFile("t.cc"));
    assertTrue(SourceFile.isCppSourceFile("t.cpp"));
    assertTrue(SourceFile.isCppSourceFile("t.cxx"));
    assertTrue(SourceFile.isCppSourceFile("t.C"));
    assertTrue(SourceFile.isCppSourceFile("t.CC"));

    assertFalse(SourceFile.isCppSourceFile("t"));
    assertFalse(SourceFile.isCppSourceFile("t.c"));
    assertFalse(SourceFile.isCppSourceFile("t.h"));
    assertFalse(SourceFile.isCppSourceFile("t.hpp"));
    assertFalse(SourceFile.isCppSourceFile("t.H"));
    assertFalse(SourceFile.isCppSourceFile("t.cx"));
  }

  @Test
  public void testCFileExtentions() {
    assertTrue(SourceFile.isCSourceFile("t.c"));

    assertFalse(SourceFile.isCSourceFile("t.cc"));
    assertFalse(SourceFile.isCSourceFile("t.cpp"));
    assertFalse(SourceFile.isCSourceFile("t.cxx"));
    assertFalse(SourceFile.isCSourceFile("t.C"));
    assertFalse(SourceFile.isCSourceFile("t.CC"));
  }
}
