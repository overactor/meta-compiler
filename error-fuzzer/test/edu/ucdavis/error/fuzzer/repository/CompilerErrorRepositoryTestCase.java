package edu.ucdavis.error.fuzzer.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/** Created by Chengnian Sun on 3/11/17. */
@RunWith(JUnit4.class)
public class CompilerErrorRepositoryTestCase {

  @Test
  public void testValidaErorrInstanceFolderName() {
    assertTrue(CompilerErrorRepository.isValidErrorInstanceFolderName("000"));
    assertTrue(CompilerErrorRepository.isValidErrorInstanceFolderName("09999"));
    assertTrue(CompilerErrorRepository.isValidErrorInstanceFolderName("0"));

    assertFalse(CompilerErrorRepository.isValidErrorInstanceFolderName("s"));
    assertFalse(CompilerErrorRepository.isValidErrorInstanceFolderName(""));
  }
}
