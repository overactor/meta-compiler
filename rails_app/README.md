# The Metacompiler Database 

Compiler error messages are cryptic, difficult to understand.  In this
application, we provide a list of error messages and, for each error message,
we provide a list of examples that trigger that error message.  We hypothesize
that by providing this meta information, developers will be able to fix the
compiler errors in their code faster.

## Installation

Dependencies:

* clang++-3.9
		* Instructions: [https://askubuntu.com/a/799998](https://askubuntu.com/a/799998)
* clang-format-3.9
* PostgreSQL
* Ruby
    * recommend using RVM
* nodejs

### Development

* Install postgresql
** sudo apt-get install libpq-dev
** create role DBUSERNAME with createdb login password 'DBPASSWORD';
** alter role DBUSERNAME with superuser;

* Install rvm
* rvm install ruby
* gem install bundler

* set values in .env.development

* rails db:create
* rails db:migrate
  * Export existing database using pg_dump
	* Import dumped database using psql 

### Initial Deployment to Production

Chengnian generates the example fixes and places them in a file called
"test.report.txt".


```
source .env.production
RAILS_ENV=production rails db:create
RAILS_ENV=production rails db:migrate
RAILS_ENV=production rails db:seed
RAILS_ENV=production rails examples:import IMPORT_FNAME=lib/tasks/test.report.txt
RAILS_ENV=production rails examples:compile
RAILS_ENV=production rails templates:import IMPORT_FNAME=lib/tasks/google-queries.txt
RAILS_ENV=production rails dms:update_template_id_clang
RAILS_ENV=production rails templates:update_diagnostic_messages_count
RAILS_ENV=production rails templates:update_examples_count
```


### Update Production

```
git pull
bundle install
source .env.production
RAILS_ENV=production rails db:migrate
RAILS_ENV=production bundle exec rake assets:precompile
sudo cp -Ru . /opt/nginx/html/metacompiler/
sudo touch /opt/nginx/html/metacompiler/tmp/restart.txt

```


### Install SSL Certificate


#### Get SSL Certificate

Resource: https://docs.google.com/document/d/1Lus2zKLcDw9Pr6C_IfwriMdRgeHmh-LPY5p8xNfgCjk/edit.

Create your CSR.

```
cd ~
mkdir ssl
cd ssl
openssl req -nodes -newkey rsa:2048 -keyout hostname.cs.ucdavis.edu.key -out hostname.cs.ucdavis.edu.csr
```
Order from InCommon.  See  https://docs.google.com/document/d/1Lus2zKLcDw9Pr6C_IfwriMdRgeHmh-LPY5p8xNfgCjk/edit.

When you receive "Enrollment Successful" email, then you have to download 1)
the Certificate only file, and 2) the Intermediate/root only files.  I created a
manual-bundle.crt file with the following command.

```
$ cat srg_cs_ucdavis_edu_cert.cer srg_cs_ucdavis_interm.cer > manual-bundle.crt
```

You have to fix manual-bundle.crt.  Split the line that looks like the
following line.  BEGIN CERTIFICATE should be prepended by 5 dashes.

```
-----END CERTIFICATE----------BEGIN CERTIFICATE-----
```

You can verify that the manual-bundle.crt is valid by using:

```
keytool -printcert -v -file manual-bundle.crt
```

#### Configure nginx

Copy the srg.ucdavis.edu.key and manual-bundle.crt files into **/etc/ssl**.

Edit the Nginx conf file.  Listen on port 443.  Specify the key and crt files.

```
(MORE)...
server {
        listen       80; 
        listen       443 ssl;
        #server_name  localhost;
        server_name           srg.cs.ucdavis.edu;
        ssl_certificate       /etc/ssl/manual-bundle.crt;
        ssl_certificate_key   /etc/ssl/srg.cs.ucdavis.edu.key;
        root   html;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        #location / {
        #    index  index.html index.htm;
        #}  

        location /hello_world/ {
          passenger_enabled on; 
          rails_env    production;
          root  html/hello_world/public;
          #index  index.html index.htm;
        }   
...(MORE)
```






### Compilers

* Family
  * compiler
* Clang
  * clang-3.9
  * clang++-3.9
* GCC
  * gcc-4.0

### Diagnostic Messages

* compiler id
* name * message 
### Examples

* diagnostic message id
* buggy  
* fixed 
* submitted by


