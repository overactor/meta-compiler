Rails.application.routes.draw do
	devise_for :users, :controllers => { :registrations => 'users/registrations', :omniauth_callbacks => "users/omniauth_callbacks" }
	# https://github.com/plataformatec/devise/wiki/How-To:-Define-resource-actions-that-require-authentication-using-routes.rb
  resources :snippets

  authenticate :user do
		get 'evaluations/consent', to: 'evaluations#consent'
		post 'evaluations/record_consent', to: 'evaluations#record_consent'
		get 'evaluations/questionnaire', to: 'evaluations#questionnaire'
		post 'evaluations/record_questionnaire', to: 'evaluations#record_questionnaire'
		get 'evaluations/tutorial', to: 'evaluations#tutorial'
		post 'evaluations/record_tutorial_quiz', to: 'evaluations#record_tutorial_quiz'
		get 'evaluations/tasks', to: 'evaluations#tasks'
		get 'evaluations/task', to: 'evaluations#task'
		post 'evaluations/task_finish', to: 'evaluations#task_finish'
		get 'evaluations/task_questionnaire', to: 'evaluations#task_questionnaire'
		post 'evaluations/record_task_questionnaire', to: 'evaluations#record_task_questionnaire'
		get 'evaluations/questionnaire_final', to: 'evaluations#questionnaire_final'
		post 'evaluations/record_questionnaire_final', to: 'evaluations#record_questionnaire_final'
		get 'evaluations/thank_you', to: 'evaluations#thank_you'
		get 'evaluations/times_report', to: 'evaluations#times_report'
  	resources :evaluations
	end

  resources :compilation_events
  resources :users
	get 'diagnostic_messages/search', to: 'diagnostic_messages#search'
	get 'examples/random', to: 'examples#random'
  resources :compilers do
  	resources :templates
  	resources :diagnostic_messages do
			get 'embed', to: 'diagnostic_messages#embed'
  		resources :examples do
				#get 'show2', to: 'examples#show2'
			end
		end
	end
  get 'welcome/index'
	post 'welcome/tokenize'	
	post 'welcome/compile'	
	get 'welcome/possible_fixes'	
	get 'welcome/autoexperiment'	
  post 'welcome/compiler'
  get 'welcome/fix_search_control'
	root 'welcome#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
