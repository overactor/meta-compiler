# This file is used by Rack-based servers to start the application.

require_relative 'config/environment'

# original
#run Rails.application

# Running app in a subdirectory in production
map Metacompiler::Application.config.relative_url_root || "/" do
	run Rails.application
end
