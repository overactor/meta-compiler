# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170829012748) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"

  create_table "compilation_events", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "code"
    t.text     "output"
    t.string   "command"
    t.text     "error"
    t.string   "event"
    t.integer  "examples_count"
    t.integer  "fixes_count"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "compilers", force: :cascade do |t|
    t.string   "family"
    t.string   "name"
    t.text     "description"
    t.string   "url"
    t.integer  "created_by"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "diagnostic_messages_count"
    t.integer  "templates_count",                default: 0,  null: false
    t.integer  "examples_count",                 default: 0,  null: false
    t.json     "summary_by_components",          default: {}
    t.json     "summary_by_selected_components", default: {}
  end

  create_table "diagnostic_messages", force: :cascade do |t|
    t.integer  "compiler_id"
    t.string   "name"
    t.text     "message"
    t.integer  "created_by"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "examples_count",           default: 0
    t.integer  "bing_search_result_count", default: 0
    t.integer  "template_id"
  end

  create_table "evaluations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "status",        default: 0
    t.string   "code"
    t.json     "questionnaire"
    t.json     "tutorial_quiz"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.json     "consent"
    t.json     "tasks",         default: {}
    t.integer  "next_task"
    t.json     "grades",        default: {}
  end

  create_table "examples", force: :cascade do |t|
    t.integer  "diagnostic_message_id"
    t.text     "buggy"
    t.text     "fixed"
    t.integer  "created_by"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "buggy_tokens",             default: [],              array: true
    t.string   "fixed_tokens",             default: [],              array: true
    t.binary   "buggy_tokens_sha1"
    t.binary   "fixed_tokens_sha1"
    t.text     "compiler_output"
    t.text     "compiler_output_fixed"
    t.integer  "token_diff_change"
    t.string   "token_diff_token"
    t.string   "patch"
    t.text     "buggy_compilation_output"
    t.integer  "buggy_compilation_status"
    t.text     "fixed_compilation_output"
    t.integer  "fixed_compilation_status"
    t.integer  "buggy_loc"
    t.integer  "fixed_loc"
    t.integer  "buggy_tokens_size"
    t.integer  "fixed_tokens_size"
  end

  create_table "snippets", force: :cascade do |t|
    t.text     "snippet"
    t.integer  "diagnostic_message_id"
    t.string   "tokens",                default: [],              array: true
    t.json     "compilation"
    t.text     "source"
    t.integer  "created_by"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.text     "notes"
  end

  create_table "templates", force: :cascade do |t|
    t.string   "name"
    t.text     "template"
    t.integer  "compiler_id"
    t.integer  "created_by"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "diagnostic_messages_count",   default: 0,  null: false
    t.integer  "examples_count",              default: 0,  null: false
    t.integer  "search_result_counts",        default: [],              array: true
    t.text     "sampled_query"
    t.integer  "sampled_search_result_count", default: 0
    t.json     "web_search",                  default: {}
    t.string   "components",                  default: [],              array: true
    t.integer  "patch_count",                 default: 0
    t.json     "patches",                     default: {}
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "email",                  default: "",    null: false
    t.string   "name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "location"
    t.string   "image_url"
    t.string   "url"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "admin",                  default: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
