class AddPatchCountToTemplate < ActiveRecord::Migration[5.0]
  def change
    add_column :templates, :patch_count, :integer, default: 0
  end
end
