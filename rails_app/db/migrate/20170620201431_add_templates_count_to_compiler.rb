class AddTemplatesCountToCompiler < ActiveRecord::Migration[5.0]
  def change
    add_column :compilers, :templates_count, :integer, default: 0, null: false
  end
end
