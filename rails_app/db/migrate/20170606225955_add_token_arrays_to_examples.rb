class AddTokenArraysToExamples < ActiveRecord::Migration[5.0]
  def change
    add_column :examples, :buggy_tokens, :string, array: true, default: []
    add_column :examples, :fixed_tokens, :string, array: true, default: []
  end
end
