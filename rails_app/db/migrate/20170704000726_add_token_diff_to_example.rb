class AddTokenDiffToExample < ActiveRecord::Migration[5.0]
  def change
		add_column :examples, :token_diff_change, :integer
		add_column :examples, :token_diff_token, :string
  end
end
