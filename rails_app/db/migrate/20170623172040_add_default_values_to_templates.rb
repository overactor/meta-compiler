class AddDefaultValuesToTemplates < ActiveRecord::Migration[5.0]
  def change
		change_column :templates, :diagnostic_messages_count, :integer, null: false, default: 0
		change_column :templates, :bing_search_result_count, :integer, null: false, default: 0
  end
end
