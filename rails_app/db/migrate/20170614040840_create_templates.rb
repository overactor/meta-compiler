class CreateTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :templates do |t|
      t.string :name
      t.text :template
      t.integer :compiler_id
      t.integer :created_by

      t.timestamps
    end
  end
end
