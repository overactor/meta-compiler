class CreateDiagnosticMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :diagnostic_messages do |t|
      t.integer :compiler_id
      t.string :name
      t.text :message
      t.integer :created_by

      t.timestamps
    end
  end
end
