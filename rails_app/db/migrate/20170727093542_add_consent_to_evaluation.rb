class AddConsentToEvaluation < ActiveRecord::Migration[5.0]
  def change
    add_column :evaluations, :consent, :json
  end
end
