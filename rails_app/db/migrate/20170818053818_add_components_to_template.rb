class AddComponentsToTemplate < ActiveRecord::Migration[5.0]
  def change
		add_column :templates, :components, :string, array: true, default: []
  end
end
