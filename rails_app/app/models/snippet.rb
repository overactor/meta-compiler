# Store real-world code snippets.
# Used to store the real-world C++ examples used in the first user study.
# We can collect human fixes for this snippets later.
# We can use the snippets for futher fuzz-and-reduce.
class Snippet < ApplicationRecord
	belongs_to :diagnostic_message
end
