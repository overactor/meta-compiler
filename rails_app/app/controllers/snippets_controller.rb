class SnippetsController < ApplicationController
	before_action :authenticate_user!, :except => [:show, :index]
  before_action :require_admin, except: [:show, :index]
  before_action :set_snippet, only: [:show, :edit, :update, :destroy]


  # GET /snippets
  # GET /snippets.json
  def index
    @snippets = Snippet.order(:id)
  end

  # GET /snippets/1
  # GET /snippets/1.json
  def show
		@snippet.save
	end

  # GET /snippets/new
  def new
    @snippet = Snippet.new
  end

  # GET /snippets/1/edit
  def edit
  end

  # POST /snippets
  # POST /snippets.json
  def create
    @snippet = Snippet.new(snippet_params)
		compile_and_tokenize()
		@snippet.snippet = @snippet.snippet.format

    respond_to do |format|
      if @snippet.save
        format.html { redirect_to @snippet, notice: 'Snippet was successfully created.' }
        format.json { render :show, status: :created, location: @snippet }
      else
        format.html { render :new }
        format.json { render json: @snippet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /snippets/1
  # PATCH/PUT /snippets/1.json
  def update
		
    respond_to do |format|
      if @snippet.update(snippet_params)
				compile_and_tokenize()
				@snippet.snippet = @snippet.snippet.format
				@snippet.save	
        format.html { redirect_to @snippet, notice: 'Snippet was successfully updated.' }
        format.json { render :show, status: :ok, location: @snippet }
      else
        format.html { render :edit }
        format.json { render json: @snippet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /snippets/1
  # DELETE /snippets/1.json
  def destroy
    @snippet.destroy
    respond_to do |format|
      format.html { redirect_to snippets_url, notice: 'Snippet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_snippet
      @snippet = Snippet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def snippet_params
      params.require(:snippet).permit(:snippet, :diagnostic_message_id, :tokens, :compilation, :source, :notes)
    end

		def compile_and_tokenize
			compiler = Compiler.find(1)
			@snippet.compilation = {} 
			@snippet.compilation = {}
			compilation = @snippet.snippet.compile
			@snippet.compilation["output"] = compilation[0]
			@snippet.compilation["status"] = compilation[1].exitstatus
			message = @snippet.compilation["output"].extract_diagnostic_message
			dm = DiagnosticMessage.find_or_create_by(compiler_id: compiler.id, message: message)
			@snippet.diagnostic_message_id = dm.id
			@snippet.tokens = @snippet.snippet.tokenize
		end
end
