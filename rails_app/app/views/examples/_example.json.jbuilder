json.extract! example, :id, :diagnostic_message_id, :buggy, :fixed, :created_by, :created_at, :updated_at
json.url example_url(example, format: :json)