json.extract! diagnostic_message, :id, :compiler_id, :name, :message, :created_by, :created_at, :updated_at
json.url diagnostic_message_url(diagnostic_message, format: :json)