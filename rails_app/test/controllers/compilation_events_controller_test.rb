require 'test_helper'

class CompilationEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @compilation_event = compilation_events(:one)
  end

  test "should get index" do
    get compilation_events_url
    assert_response :success
  end

  test "should get new" do
    get new_compilation_event_url
    assert_response :success
  end

  test "should create compilation_event" do
    assert_difference('CompilationEvent.count') do
      post compilation_events_url, params: { compilation_event: { code: @compilation_event.code, command: @compilation_event.command, error: @compilation_event.error, event: @compilation_event.event, examples_count: @compilation_event.examples_count, fixes_count: @compilation_event.fixes_count, output: @compilation_event.output, user_id: @compilation_event.user_id } }
    end

    assert_redirected_to compilation_event_url(CompilationEvent.last)
  end

  test "should show compilation_event" do
    get compilation_event_url(@compilation_event)
    assert_response :success
  end

  test "should get edit" do
    get edit_compilation_event_url(@compilation_event)
    assert_response :success
  end

  test "should update compilation_event" do
    patch compilation_event_url(@compilation_event), params: { compilation_event: { code: @compilation_event.code, command: @compilation_event.command, error: @compilation_event.error, event: @compilation_event.event, examples_count: @compilation_event.examples_count, fixes_count: @compilation_event.fixes_count, output: @compilation_event.output, user_id: @compilation_event.user_id } }
    assert_redirected_to compilation_event_url(@compilation_event)
  end

  test "should destroy compilation_event" do
    assert_difference('CompilationEvent.count', -1) do
      delete compilation_event_url(@compilation_event)
    end

    assert_redirected_to compilation_events_url
  end
end
