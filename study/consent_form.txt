The Metacompiler Study Consent Form 
===================================
Version: 21july2017

You are being asked to take part in an evaluation of a new programming tool that may help developers resolve source code defects.  We are asking you to take part because you have some C++ programming experience.  Please read this form carefully and ask any questions you may have before agreeing to take part in the study.


What the study is about: The purpose of this study is to evaluate a new programming tool that may help developers resolve compiler errors. 


What we will ask you to do: If you agree to be in this study, we will give you a series of C++ debugging tasks.  For each task, we will give you a C++ code snippet and ask you to fix the code defect indicated by the compiler.  The screen will be recorded.  The study will last approximately 1 hour.


Risk and benefits: We do not anticipate any risks to you participating in this study other than those encountered in day-to-day life.  There are no immediate benefits to you.


Compensation: You will be given a $15 Amazon gift card.  The gift card will be emailed to you by the department near the end of the month.


Your answers will be confidential: The records of this study will be kept private.  In any sort of report we make public we will not include any information that will make it possible to identify you.  Research records will be kept in password-protected files; only the researchers will have access to the records.


Taking part is voluntary: Taking part in this study is completely voluntary.  You may skip any tasks that you do not want to do.  If you decide to take part, you are free to withdraw at any time.


If you have questions: The researchers conducting this study are Martin Velez and Prof. Zhendong Su.  Please ask any questions you have now.  If you have questions later, you may contact Martin Velez at marvelez@ucdavis.edu or at (209) 292-4439.  You can reach Prof. Zhendong Su at su@ucdavis.edu.


You will be given a copy of this form to keep for your records.


Statement of Consent: I have read the above information, and have received answers to any questions I asked.  I consent to take part in the study.


_________________________________							_________________________________
Your Name (Printed)											Researcher's Name (Printed)

_________________________________							_________________________________
Your Signature												Researcher's Signature

_________________________________							_________________________________
Date														Date

_________________________________
Email for Gift Card


