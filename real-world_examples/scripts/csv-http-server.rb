require 'webrick'

root = File.expand_path '~/public_html'
server = WEBrick::HTTPServer.new :Port => 7666, :DocumentRoot => root


trap 'INT' do server.shutdown end


server.mount_proc '/' do |req, res|
  res['Content-Type'] = "text/html"

  res.body += "<ol>"
  File.read("../100examples.csv").each_line.to_a.drop(1).each do |row|
    row =~ /\d+, [0-9a-z]+, (.*)/
    msg = $1
    res.body += %Q[<li><a href="https://google.com/search?q=#{msg}">#{msg}</a></li>]
    msg2 = msg.gsub(/'[^']+'/, '')
    res.body += %Q[<ul><li>&nbsp;<a href="https://google.com/search?q=#{msg2}">#{msg2}</a></li></ul>]
  end
  res.body += "</ol>"
end

server.start
