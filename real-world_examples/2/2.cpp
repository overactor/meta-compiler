struct mutex { };

mutex& get();

struct guard {
  guard(mutex&) { }
};

void f()
{
  guard(get());
}

