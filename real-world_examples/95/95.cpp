template< typename A >
struct Foo
{
    const A& a_;
    Foo( const A& a ) : a_( a ) { };
};

int main() {
    int myval = 0;
    Foo< int > foo( myval );
    Foo foo( myval );
}
