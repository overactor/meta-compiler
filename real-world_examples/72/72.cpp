template <typename T, int N>
struct Vector {
    T data[N];
};

struct Dynamic {
};

template <typename T>
struct Vector<T, Dynamic> {
    T* data;
};