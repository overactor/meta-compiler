#include "Vector.h"       //get the interface
//To support the range-for loop for our Vector, we must define suitable begin() and end() functions:
template <typename T>
const T* Vector<T>::begin(Vector<T>& x)
{
    return & x[0];
}
template <typename T>
const T* Vector<T>::end(Vector<T>& x)
{
    return x.begin()+x.size(); //pointer to one past last element.
}