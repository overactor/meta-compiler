void test_result_type(int N) {
  typedef int vla[N];
  auto l2 = [] () -> vla { }; // expected-error{{function cannot return array type 'vla' (aka 'int [N]')}}
}
