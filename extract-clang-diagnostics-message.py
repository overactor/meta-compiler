#!/usr/bin/env python2.7


import sys
import os.path

CLANG_RAW_MSG_FILES = [os.path.join("clang-raw-diagnostics-messages/", x) for x
                       in ["DiagnosticCommentKinds.inc",
                           "DiagnosticCommonKinds.inc",
                           "DiagnosticDriverKinds.inc",
                           "DiagnosticFrontendKinds.inc",
                           "DiagnosticLexKinds.inc",
                           "DiagnosticParseKinds.inc",
                           "DiagnosticSemaKinds.inc",
                           "DiagnosticSerializationKinds.inc"]]

RESULT_FILE_NAME = "all-clang-messages.txt"


class ClangMsg:
    @staticmethod
    def segment_diag_line(diag_msg):
        indices = []
        diag_msg.index("(")
        indices.append()
        return indices

    def __init__(self, diag_msg):
        # DIAG(err_fe_backend_plugin, CLASS_ERROR, (unsigned)diag::Severity::Error, "%0", 0, SFINAE_SubstitutionFailure, false, true, 14)
        self.msg = diag_msg;
        # extract id
        left_par_index = diag_msg.index("(")
        first_comma_index = diag_msg.index(",", left_par_index + 1)
        self.msg_id = diag_msg[left_par_index + 1:first_comma_index].strip()

        # extract class type
        second_comma_index = diag_msg.index(",", first_comma_index + 1)
        self.class_type = diag_msg[first_comma_index + 1:second_comma_index]

        # extract severity
        third_comma_index = diag_msg.index(",", second_comma_index + 1)
        self.severity = diag_msg[second_comma_index + 1:third_comma_index]

        # extract message
        right_par_index = diag_msg.rindex(")")
        eighth_comma_index = diag_msg.rindex(",", 0, right_par_index)
        seventh_comma_index = diag_msg.rindex(",", 0, eighth_comma_index)
        sixth_comma_index = diag_msg.rindex(",", 0, seventh_comma_index)
        fifth_comma_index = diag_msg.rindex(",", 0, sixth_comma_index)
        fourth_comma_index = diag_msg.rindex(",", 0, fifth_comma_index)

        self.content = diag_msg[third_comma_index + 1:fourth_comma_index]


ALL_CLANG_MSGs = []

for raw_msg_file in CLANG_RAW_MSG_FILES:
    with open(raw_msg_file, "r") as f:
        for line in f.readlines():
            line = line.rstrip('\n').rstrip('\r')
            if not line.startswith("DIAG"):
                continue
            ALL_CLANG_MSGs.append(ClangMsg(line))

with open(RESULT_FILE_NAME, "w") as f:
    for clang_msg in ALL_CLANG_MSGs:
        f.write(clang_msg.msg_id)
        f.write("\t")
        f.write(clang_msg.severity)
        f.write("\t")
        f.write(clang_msg.content)
        f.write("\n\n")
