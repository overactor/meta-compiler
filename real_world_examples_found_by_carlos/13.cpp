#include<iostream>
#include<map>

namespace TheNS {

    class Entity;

    struct EntityState
    {
        std::string aString, anotherString;
        int anInt;

        EntityState() {}

        EntityState(std::string a, std::string b, int i)
        {
            // constructor
        }
    };

	// FIX: delete typedef or change indentifier name
    typedef std::map<std::string, EntityState> Entity;

    class Entity
    {
    public:
        void SomeFunction();

    private:
        int m_aVar;

    };

}    