#!/usr/bin/env ruby 


errors = []
queries = []
count = 0
f = File.open('google-queries.txt', 'r')
while(line = f.gets)
	if line =~ /Query for error/
		errors << line.split[3]		
	end
	if line =~ /Google query segments:/
		count = count + 1
		q = ""
		while line = f.gets
			if !line.strip.empty?
				q = q + " " + "\"#{line.strip}\""
			else
				break
			end
		end		
		queries << q.lstrip	
	end
end

of = File.open('queries.txt', 'w')
queries.each do |q|
	of.puts q
end
of.close

of = File.open('errors.txt', 'w')
errors.each do |e|
	of.puts e
end
of.close
