# Paper

This is the Latex source code for our paper.

## Building

On MacOS, install Skim.

Configure latexmk. On macOS, you can also use $HOME/.latexmkrc, e.g. with this content:

```
$pdf_previewer = 'open -a Skim';
$pdflatex = 'pdflatex -synctex=1 -interaction=nonstopmode';
@generated_exts = (@generated_exts, 'synctex.gz');
```

To build the paper we use the make system.

```
make clean
make
```
