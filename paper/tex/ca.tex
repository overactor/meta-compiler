\section{Online Search}
\label{sec:online}

By sampling the repair search space ahead of time, the list of candidate
repairs has been reduced. However, a single error message may contain tens,
hundreds, even thousands of repair examples.  This leads to the
\emph{information retrieval problem} of retrieving and ranking compilation
repair examples.  In this section, we describe \textsc{CompAssist}'s search
engine, outlined in \autoref{fig:arch}.  As a \emph{query}, the search engine
is given 1) an arbitrary user program, and 2) a compiler name.  The search
engine compiles the program using the specified compiler.  If compilation
succeeds, the search engine returns an empty result.  Otherwise, it proceeds to
retrieve and rank related example fixes.  

\textbf{Retrieval:}
For an uncompilable program that triggers some error message $e$, we say that
\emph{related compiler repair examples} are those that trigger the same error
message $e$ or the same \emph{kind of error}.  The complete set of possible
kinds of errors and error message templates can usually be found in the source
code of the compiler.  In practice, the set can be inferred and approximated
from observed error messages.
% DO NOT LSTINLINE text into new lines. 
Given an error message $e$, we map it to its kind, and we retrieve all repair
examples of the same kind.
For example, if $e$ is ``\lstinline[basicstyle=\ttfamily]{use of undeclared 'x'}'', 
then it matches ``\texttt{use of undeclared \%0}'' so we retrieve all
example fixes for \lstinline[basicstyle=\ttfamily]{err_undeclared_var_use}
which includes examples for ``\lstinline[basicstyle=\ttfamily]{use of undeclared 'y'}''.

\textbf{Patch Inference:}
In general, a patch is just the difference between two texts.  And it is
difficult to infer the high-level syntactic transformations from a low-level
patch~\cite{dantoni2017fse,long2016popl,long2017fse,rolim2017icse}.  Since we
synthesize the repair examples, we obviate this problem.  For each repair
example, we know the exact high-level patch (or transformation).  This is due
to the fact that, in generating example fixes, we fuzz at a ``high-level''
representation of the source code.  For example, in \autoref{fig:t10}, we show
example fixes grouped by patch.  The first group of example fixes contains
example fixes where the patch is ``Insert public''.  To support
user-contributed example fixes, we would have to implement patch inference.

\textbf{Ranking:}
\label{sec:ranking}
We aim 1) to present plausible and ``acceptable'' possible patches first, and
2) to present repair examples most similar to the user program first in each
patch group.  
First, we score each of the related repair examples.  With input user program
$u_{user}$, to score a repair example $(u,c)$, we compute the overlap
coefficient~\cite{manning1999} and subtract the Levenshtein
distance~\cite{jurafsky2009} between $u_{user}$ and $u$.
\begin{equation*}
\mathit{score}\left(u,u_{user}\right) = \mathit{overlap}\left(u,u_{user}\right) - \mathit{lev\_dist}\left(u,u_{user}\right)
\end{equation*}
Second, we assign a preliminary score to each patch.  Let $RE_p$ denote a group
of repair examples that exhibit patch $p$.  To score a patch $p$ with respect
to a user program $u_{user}$, we assign it the maximum score of its
corresponding examples $RE_p$.
\begin{equation*}
\mathit{score}(p,u_{user}) = max(\{\mathit{score}\left(u,u_{user}\right)\ |\ \left(u,c\right) \in RE_p\})
\end{equation*}
At this stage, the search engine returns a ranked list of patches where each
patch has a ranked list of example fixes.
This helps ensure that the time-to-interaction (TTI) is low.

\textbf{Auto-experimentation:}
We test each patch on the user program using brute-force.  It updates the score
of each patch if any experiments resulted in a successful compilation.  Based
on pilot studies, auto-experimentation improves the ranking of the patch
suggestions.  
For example, suppose a possible patch is ``Insert comma'' (ignoring location).  
Then, we apply this patch to every possible location of the tokenized user
program checking if the patched program compiles.  
We record experiments that result in successful compilations. 
``Delete TOKEN'' transformations are easier to experiment with because we only
have to delete the instances of the token type indicated by the patch.   For
example, if a patch is ``Delete amp'', then we only have to delete the ``\&''
instances of the user program.  If the user program does not have any ``\&'',
then no experimation is needed and the score remains the same.
